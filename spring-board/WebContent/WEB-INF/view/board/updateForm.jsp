<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
th, td {
    border:1px solid black;
    berder-collapse:collapse;
}
th {
    background-color: #dedede;
}
table tr:last-child td {
    border:0px solid black;
    text-align:right;
}

</style>
</head>
<body>
    <form action="boardInsertOk" method="post">
        <table>
            <tr>
                <th>작성자</th>
                <td><input type="text" name="writer" value="${dto.writer }"/></td>
            </tr>
            <tr>
                <th>제목</th>
                <td><input type="text" name="title" value="${dto.title }"/></td>
            </tr>
            <tr>
                <th>내용</th>
                <td><textarea cols="40" rows="7" name="contents">${dto.contents }</textarea></td>
            </tr>
            <tr>
                <th>IP</th>
                <td><input type="text" name="ip" value="${dto.ip }"/></td>
            </tr>
            <tr>
                <th>조회수</th>
                <td><input type="text" name="readcount" value="${dto.readcount }"/></td>
            </tr>
            <tr>
                <th>추천수</th>
                <td><input type="text" name="hits" value="${dto.hits }"/></td>
            </tr>
            <tr>
                <th>등록일</th>
                <td><input type="text" name="regdate" value="${dto.regdate }"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="등록"/>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
package kr.co.hbilab.app;

import java.util.List;

//20150526
public interface Dao {
    public List<EmpDTO> selectAll();
    public EmpDTO selectOne(int no);
    public void insertOne(EmpDTO dto);
    public void updateOne(EmpDTO dto);
    public void deleteOne(int no);
}

package kr.co.hbilab.app;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

public class TestMain{
    public static void main(String[] args) {
        
        BeanFactory factory = new XmlBeanFactory(new FileSystemResource("src/app.xml"));
        
        Message m = factory.getBean("message", Message.class);
        m.sayHello();
        
    }
}
